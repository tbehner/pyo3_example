#![feature(use_extern_macros, specialization)]
extern crate pyo3;

use pyo3::prelude::*;
use std::process::Command;

// Create a python class from a rust struct
#[pyclass(dict)]
struct Echoer {
    input: String,
    // has to contain a PyToken
    token: PyToken,
}

#[pymethods]
impl Echoer {
    // Implement the __new__ function instead of __init__
    #[new]
    fn __new__(obj: &PyRawObject, input: String) -> PyResult<()> {
        obj.init(|t| 
                 Echoer { 
                     input: input,
                     token: t 
                    })
    }

    // Regular functions can be implemented as usual but every function has to have a `Python`
    // argument
    fn echo(&self, _py: Python) -> PyResult<String> {
        Ok(self.input.clone())
    }
}


/// A package for interacting with Virtual Machines on the KVM
#[pymodinit(pyo3_example)]
fn init_mod(_py: Python, m: &PyModule) -> PyResult<()> {

    // This adds a function to the python module:
    /// Returns the content of the directory as a string seperated by newlines
    #[pyfn(m, "ls")]
    fn ls(path: String) -> PyResult<String> {
        let result = Command::new("ls")
            .arg(path)
            .output()?;

        if !result.status.success() {
            return Err(exc::ValueError::new(String::from_utf8_lossy(&result.stderr).to_string()))
        }

        Ok(String::from_utf8_lossy(&result.stdout).to_string())
    }

    // This adds a class to the python module:
    m.add_class::<Echoer>()?;

    Ok(())
}
