import sys
sys.path.append('./target/debug/')
import pyo3_example
import pytest

def test_ls():
    with pytest.raises(Exception) as _:
        pyo3_example.ls('/root')
    assert '/home' != ''

def test_echoer():
    e = pyo3_example.Echoer('Hello')
    assert e.echo() == 'Hello'
