build-lib:
    cargo build

link:
    #!/usr/bin/env python3
    import subprocess
    import re
    from pathlib import Path
    p = Path.cwd() / 'target' / 'debug'
    so_file_path = list(p.glob('*.so'))[0]
    link_filename = re.sub('^lib', '', so_file_path.name)
    so_link = so_file_path.parent / link_filename
    if not so_link.exists():
        so_link.symlink_to(so_file_path)

build:
    pyo3-pack build -d

install-fedora-dependencies:
    sudo dnf install dbus-devel gmp-devel openssl-devel

install-dependencies:
    #!/usr/bin/env python3
    import subprocess
    ret = subprocess.run("which pyo3-pack", shell=True)
    if ret.returncode != 0:
        print("Installing pyo3-pack")
        subprocess.run("cargo install pyo3-pack", shell=True)

test: build-lib link
    pytest
